#ifndef DEMO_H
#define DEMO_H

using namespace std;



class Investment 
{
    private:  //properties
         long double value;
         float interest_rate; 
         double initial_investment;
         double monthly_investment;
         double A;
         double R;
         double L;
         double afterLogL;
         double afterLogR;
         double Both;
         double Time;



    public:  

          void setinitial_investment(double);

          void setinterest_rate(float);

          void setmonthly_investment(double);
          
          double getmonthly_investment();

          float getinterest_rate();
   
          double getinitial_investment();
          
          void setvalue(double);

          double getvalue();

          void calculations();

          void right_side();

          void left_side();

          void dividing_both();
           
          void Estimated_Time();
};

#endif