#include<iostream>
#include<bits/stdc++.h>
#include "Demo.h"
using namespace std;

    
          void Investment::setinitial_investment(double vest){
              initial_investment = vest;
          }

          void Investment::setinterest_rate(float rate){
              interest_rate = rate;
          }

          void Investment::setmonthly_investment(double m)
          {
              monthly_investment = m;
          }
          
          double Investment::getmonthly_investment()
          {
              return monthly_investment;
          }

          float Investment::getinterest_rate(){
             return interest_rate;
          }
   
          double Investment::getinitial_investment(){
              return initial_investment;
          }
          
          void Investment::setvalue(double val){
              value = val;
          }

          double Investment::getvalue(){
            return value;
          }

          void Investment::calculations(){
            A = (monthly_investment*12) + initial_investment;
            cout << "The final amount 'A' = (P+(I*12)): "<< A << endl;
          }

          void Investment::right_side(){
            R = 1 + (interest_rate/12);
            afterLogR = log(R);
            cout <<"1+(Interest rate / 12): "<< R << endl;
            cout <<"log(1+R/n) is the formula used to return this value: "<< afterLogR <<endl;
          }
          void Investment::left_side(){
            L = A / initial_investment;
            afterLogL = log(L);
            cout <<"A / Initial investment: "<< L << endl;
            cout <<"log(A/P) is the formula used to return this value: "<< afterLogL << endl;
          }
        void Investment::dividing_both(){
           Both = afterLogL/afterLogR;
           cout << "Both Sides of the formula Divided->(log A/P) / log(1+R/n): " << Both << endl;
        }
        void Investment::Estimated_Time(){
          Time = round(Both/12);
          cout << "The Estimated time: " << Time <<" years" << endl;
        }

