#include<iostream>
#include<string>
#include "Demo.h"

int main()
 {
     cout << "Formula: A = P(1+r/n)^nt"<<endl;
     Investment object;
     object.setinitial_investment(100);
     object.setinterest_rate(0.083);
     object.setmonthly_investment(250);
     object.setvalue(1000000);
     cout <<"Initial Investment(P): $" << object.getinitial_investment() << endl;
     cout <<"Monthly Investment(I): $" << object.getmonthly_investment() << endl;
     cout <<"Interest Rate(r):" << object.getinterest_rate() << endl;
     cout <<"Estimated value mark:  $" << object.getvalue() << endl;


    object.calculations();
    object.right_side();
    object.left_side();
    object.dividing_both();
    object.Estimated_Time();


     return 0;
    
 }
